package cwy.myapplication;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new CanvasView(this));
    }

    protected class CanvasView extends View {
        public CanvasView(Context context) {
            super(context);
        }
        public void onDraw(Canvas canvas) {
            canvas.drawColor(Color.LTGRAY);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

            paint.setColor(Color.YELLOW);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, 200, paint);

            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(20);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, 200, paint);

            paint.setStyle(Paint.Style.FILL);
            canvas.drawCircle(getWidth() / 2 - 100, getHeight() / 2 - 80, 20, paint);

            canvas.drawCircle(getWidth() / 2 + 100, getHeight() / 2 - 80, 20, paint);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(20);
            RectF rect = new RectF(getWidth() / 2 - 100, getHeight() / 2 + 10, getWidth() / 2 + 100, getHeight() / 2 + 140);
            canvas.drawArc(rect, 0, 180, true, paint);

        }
    }
}
